/**
 * Implements a class representing a 2d grid of cells.
 *      - New cells are initialized to Cell::DEAD.
 *      - Grids can be resized while retaining their contents in the remaining area.
 *      - Grids can be rotated, cropped, and merged together.
 *      - Grids can return counts of the alive and dead cells.
 *      - Grids can be serialized directly to an ascii std::ostream.
 *
 * You are encouraged to use STL container types as an underlying storage mechanism for the grid cells.
 *
 * @author 969197
 * @date March, 2020
 */
#include <vector>
#include <iostream>
#include "grid.h"
#include <stdexcept>


// Include the minimal number of headers needed to support your implementation.
// #include ...

/**
 * Grid::Grid()
 *
 * Construct an empty grid of size 0x0.
 * Can be implemented by calling Grid::Grid(square_size) constructor.
 *
 * @example
 *
 *      // Make a 0x0 empty grid
 *      Grid grid;
 *
 */

Grid::Grid(){
    width = 0;
    height = 0;
    total_cells = 0;
    alive_cells = 0;
    dead_cells = 0;
}

/**
 * Grid::Grid(square_size)
 *
 * Construct a grid with the desired size filled with dead cells.
 * Single value constructors should be marked "explicit" to prevent them
 * being used to implicitly cast ints to grids on construction.
 *
 * Can be implemented by calling Grid::Grid(width, height) constructor.
 *
 * @example
 *
 *      // Make a 16x16 grid
 *      Grid x(16);
 *
 *      // Also make a 16x16 grid
 *      Grid y = Grid(16);
 *
 *      // This should be a compiler error! We want to prevent this from being allowed.
 *      Grid z = 16;
 *
 * @param square_size
 *      The edge size to use for the width and height of the grid.
 */

Grid::Grid(int square_size){
    this->width = square_size;
    this->height = square_size;
    this->total_cells = square_size*square_size;
    this->dead_cells = square_size*square_size;
    this->alive_cells = 0;
    this->make_vector();
}


/**
 * Grid::Grid(width, height)
 *
 * Construct a grid with the desired size filled with dead cells.
 *
 * @example
 *
 *      // Make a 16x9 grid
 *      Grid grid(16, 9);
 *
 * @param width
 *      The width of the grid.
 *
 * @param height
 *      The height of the grid.
 */

Grid::Grid(int width, int height){
    this->width = width;
    this->height = height;
    this->total_cells = width*height;
    this->alive_cells = 0;
    this->make_vector();
}

/**
 * Grid::get_width()
 *
 * Gets the current width of the grid.
 * The function should be callable from a constant context.
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Print the width of the grid to the console
 *      std::cout << grid.get_width() << std::endl;
 *
 *      // Should also be callable in a constant context
 *      const Grid &read_only_grid = grid;
 *
 *      // Print the width of the grid to the console
 *      std::cout << read_only_grid.get_width() << std::endl;
 *
 * @return
 *      The width of the grid.
 */
const int Grid::get_width() const{
    return this->width;
}

/**
 * Grid::get_height()
 *
 * Gets the current height of the grid.
 * The function should be callable from a constant context.
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Print the height of the grid to the console
 *      std::cout << grid.get_height() << std::endl;
 *
 *      // Should also be callable in a constant context
 *      const Grid &read_only_grid = grid;
 *
 *      // Print the height of the grid to the console
 *      std::cout << read_only_grid.get_height() << std::endl;
 *
 * @return
 *      The height of the grid.
 */
const int Grid::get_height() const{
    return this->height;
}

/**
 * Grid::get_total_cells()
 *
 * Gets the total number of cells in the grid.
 * The function should be callable from a constant context.
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Print the total number of cells on the grid to the console
 *      std::cout << grid.get_total_cells() << std::endl;
 *
 *      // Should also be callable in a constant context
 *      const Grid &read_only_grid = grid;
 *
 *      // Print the total number of cells on the grid to the console
 *      std::cout << read_only_grid.get_total_cells() << std::endl;
 *
 * @return
 *      The number of total cells.
 */

const int Grid::get_total_cells() const{
    return this->total_cells;
}

/**
 * Grid::get_alive_cells()
 *
 * Counts how many cells in the grid are alive.
 * The function should be callable from a constant context.
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Print the number of alive cells to the console
 *      std::cout << grid.get_alive_cells() << std::endl;
 *
 *      // Should also be callable in a constant context
 *      const Grid &read_only_grid = grid;
 *
 *      // Print the number of alive cells to the console
 *      std::cout << read_only_grid.get_alive_cells() << std::endl;
 *
 * @return
 *      The number of alive cells.
 */
const int Grid::get_alive_cells() const {
    return this->alive_cells;
}

/**
 * Grid::get_dead_cells()
 *
 * Counts how many cells in the grid are dead.
 * The function should be callable from a constant context.
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Print the number of dead cells to the console
 *      std::cout << grid.get_dead_cells() << std::endl;
 *
 *      // Should also be callable in a constant context
 *      const Grid &read_only_grid = grid;
 *
 *      // Print the number of dead cells to the console
 *      std::cout << read_only_grid.get_dead_cells() << std::endl;
 *
 * @return
 *      The number of dead cells.
 */
const int Grid::get_dead_cells() const {
    return this->total_cells - this->alive_cells;
}

/**
 * Grid::resize(square_size)
 *
 * Resize the current grid to a new width and height that are equal. The content of the grid
 * should be preserved within the kept region and padded with Grid::DEAD if new cells are added.
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Resize the grid to be 8x8
 *      grid.resize(8);
 *
 * @param square_size
 *      The new edge size for both the width and height of the grid.
 */

void Grid::resize(int square_size){
    this->resize(square_size, square_size);
}

/**
 * Grid::resize(width, height)
 *
 * Resize the current grid to a new width and height. The content of the grid
 * should be preserved within the kept region and padded with Grid::DEAD if new cells are added.
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Resize the grid to be 2x8
 *      grid.resize(2, 8);
 *
 * @param new_width
 *      The new width for the grid.
 *
 * @param new_height
 *      The new height for the grid.
 */

void Grid::resize(int width, int height){
    int old_width = this->width;
    int old_height = this->height;
    std::vector <Cell> old_gvec = this->gvec;
    *this = Grid(width, height);

    if (!old_gvec.empty()){
    std::vector<Cell> tvec(width * height, Cell::DEAD);
        for (int i = 0; i < width; i++){
            for (int j = 0; j < height; j++){
                if ((old_gvec[(j*old_width)+i] == ALIVE || old_gvec[(j*old_width)+i] == DEAD) && ((i < old_width) && (j < old_height))){
                    tvec[(j*width)+i] = old_gvec[(j*old_width)+i];
                    if (tvec[(j*width)+i] == ALIVE){
                        this->alive_cells++;
                        this->dead_cells--;
                    }
                }
            }   
        } 
        gvec.clear();
        gvec = tvec;
    }
}

/**
 * Grid::get_index(x, y)
 *
 * Private helper function to determine the 1d index of a 2d coordinate.
 * Should not be visible from outside the Grid class.
 * The function should be callable from a constant context.
 *
 * @param x
 *      The x coordinate of the cell.
 *
 * @param y
 *      The y coordinate of the cell.
 *
 * @return
 *      The 1d offset from the start of the data array where the desired cell is located.
 */

const int Grid::get_index(int x, int y) const{
    return ((y*width)+x);
}

/**
 * Grid::get(x, y)
 *
 * Returns the value of the cell at the desired coordinate.
 * Specifically this function should return a cell value, not a reference to a cell.
 * The function should be callable from a constant context.
 * Should be implemented by invoking Grid::operator()(x, y).
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Read the cell at coordinate (1, 2)
 *      Cell cell = grid.get(1, 2);
 *
 * @param x
 *      The x coordinate of the cell to update.
 *
 * @param y
 *      The y coordinate of the cell to update.
 *
 * @return
 *      The value of the desired cell. Should only be Grid::ALIVE or Grid::DEAD.
 *
 * @throws
 *      std::exception or sub-class if x,y is not a valid coordinate within the grid.
 */

const Cell Grid::get(int x, int y) const{
    if (x < 0 || y < 0 || x > this->width || y > this->height){
        throw std::invalid_argument("invalid argument");
    }
    return operator()(x,y);
}

/**
 * Grid::set(x, y, value)
 *
 * Overwrites the value at the desired coordinate.
 * Should be implemented by invoking Grid::operator()(x, y).
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Assign to a cell at coordinate (1, 2)
 *      grid.set(1, 2, Cell::ALIVE);
 *
 * @param x
 *      The x coordinate of the cell to update.
 *
 * @param y
 *      The y coordinate of the cell to update.
 *
 * @param value
 *      The value to be written to the selected cell.
 *
 * @throws 
 *      std::exception or sub-class if x,y is not a valid coordinate within the grid.
 */

void Grid::set(int x, int y, Cell value) {
    if (x < 0 || y < 0 || x > this->width || y > this->height){
        throw std::invalid_argument("invalid argument");
    }
    this->operator()(x,y) = value;
}

/**
 * Grid::crop(x0, y0, x1, y1)
 *
 * Extract a sub-grid from a Grid.
 * The cropped grid spans the range [x0, x1) by [y0, y1) in the original grid.
 * The function should be callable from a constant context.
 *
 * @example
 *
 *      // Make a grid
 *      Grid y(4, 4);
 *
 *      // Crop the centre 2x2 in y, trimming a 1 cell border off all sides
 *      Grid x = y.crop(x, 1, 1, 3, 3);
 *
 * @param x0
 *      Left coordinate of the crop window on x-axis.
 *
 * @param y0
 *      Top coordinate of the crop window on y-axis.
 *
 * @param x1
 *      Right coordinate of the crop window on x-axis (1 greater than the largest index).
 *
 * @param y1
 *      Bottom coordinate of the crop window on y-axis (1 greater than the largest index).
 *
 * @return
 *      A new grid of the cropped size containing the values extracted from the original grid.
 *
 * @throws
 *      std::exception or sub-class if x0,y0 or x1,y1 are not valid coordinates within the grid
 *      or if the crop window has a negative size.
 */
Grid Grid::crop (int x0, int y0, int x1, int y1){
    if (x0 < 0 || y0 < 0 || x1 > this->width || y1 > this->height
    || y0 > y1 || x0 > x1){
        throw std::invalid_argument("negative argument");
    }
    Grid *old_grid = this;
    int old_width = this->width;
    int n_width = x1-x0;
    int n_height = y1-y0;
    Grid y = Grid(n_width, n_height);

    std::vector<Cell> tvec(n_width * n_height, Cell::DEAD);
    y.alive_cells = 0;
    y.dead_cells = n_width*n_height;
    for (int j = 0; j < n_height; j++){
        for (int i = 0; i < n_width; i++){
            tvec[(j*n_width)+i] = old_grid->gvec[(j+y0)*(old_width)+(i+x0)] ;
            if (tvec[(j*n_width)+i] == ALIVE){
                y.alive_cells++;
            } else {
                y.dead_cells++;
            }
        }   
    } 
    y.gvec.clear();
    y.dead_cells = y.total_cells - y.alive_cells;
    y.gvec = tvec;
    return y;
}

/**
 * Grid::operator()(x, y)
 *
 * Gets a read-only reference to the value at the desired coordinate.
 * The operator should be callable from a constant context.
 * Should be implemented by invoking Grid::get_index(x, y).
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Constant reference to a grid (does not make a copy)
 *      const Grid &read_only_grid = grid;
 *
 *      // Get access to read a cell at coordinate (1, 2)
 *      Cell cell = read_only_grid(1, 2);
 *
 * @param x
 *      The x coordinate of the cell to access.
 *
 * @param y
 *      The y coordinate of the cell to access.
 *
 * @return
 *      A read-only reference to the desired cell.
 *
 * @throws
 *      std::exception or sub-class if x,y is not a valid coordinate within the grid.
 */

const Cell& Grid::operator()(int x, int y) const {
    if (x < 0 || y < 0 || x > this->width || y > this->height){
        throw std::invalid_argument("invalid argument");
    }
    return this->gvec[get_index(x,y)];
}

Cell& Grid::operator()(int x, int y) {
    if (x < 0 || y < 0 || x > this->width || y > this->height){
        throw std::invalid_argument("invalid argument");
    }
    if (this->gvec[get_index(x,y)]){
        this->alive_cells++;
        //this->dead_cells--;
    } else { 
        this->alive_cells--;
        //this->dead_cells++;
    }
    return this->gvec[get_index(x,y)];
}

/**
 * Grid::merge(other, x0, y0, alive_only = false)
 *
 * Merge two grids together by overlaying the other on the current grid at the desired location.
 * By default merging overwrites all cells within the merge reason to be the value from the other grid.
 *
 * Conditionally if alive_only = true perform the merge such that only alive cells are updated.
 *      - If a cell is originally dead it can be updated to be alive from the merge.
 *      - If a cell is originally alive it cannot be updated to be dead from the merge.
 *
 * @example
 *
 *      // Make two grids
 *      Grid x(2, 2), y(4, 4);
 *
 *      // Overlay x as the upper left 2x2 in y
 *      y.merge(x, 0, 0);
 *
 *      // Overlay x as the bottom right 2x2 in y, reading only alive cells from x
 *      y.merge(x, 2, 2, true);
 *
 * @param other
 *      The other grid to merge into the current grid.
 *
 * @param x0
 *      The x coordinate of where to place the top left corner of the other grid.
 *
 * @param y0
 *      The y coordinate of where to place the top left corner of the other grid.
 *
 * @param alive_only
 *      Optional parameter. If true then merging only sets alive cells to alive but does not explicitly set
 *      dead cells, allowing whatever value was already there to persist. Defaults to false.
 *
 * @throws
 *      std::exception or sub-class if the other grid being placed does not fit within the bounds of the current grid.
 */
void Grid::merge(Grid other, int x0, int y0){
    merge(other, x0, y0, false);
}

void Grid::merge(Grid other, int x0, int y0, bool alive_only){
    const int o_width = other.get_width();
    const int o_height = other.get_height();
    if (x0 < 0 || y0 < 0 || x0 + other.get_width() > this->get_width()
     || y0+other.get_height() > this->get_height()){
        throw std::invalid_argument("invalid argument");
    }
    if (!this->gvec.empty()){
        std::vector<Cell> tvec = this->gvec;
        for (int i = 0; i < o_width; i++){
            for (int j = 0; j < o_height; j++){
                if (other.gvec[(i*o_width)+j] == ALIVE && (tvec[(i+x0)*width+(j+y0)] == DEAD)){
                    tvec[(i+x0)*width+(j+y0)] = other.gvec[(i*o_width)+j];
                    this->alive_cells++;
                    this->dead_cells--;
                }
                if (other.gvec[(i*o_width)+j] != ALIVE && (!alive_only) && (tvec[(i+x0)*width+(j+y0)] == ALIVE)){
                    tvec[(i+x0)*width+(j+y0)] = other.gvec[(i*o_width)+j];
                    this->dead_cells++;
                    this->alive_cells--;
                }
            }   
        } 
        this->gvec.clear();
        this->gvec = tvec;
    } 
}

/**
 * Grid::rotate(rotation)
 *
 * Create a copy of the grid that is rotated by a multiple of 90 degrees.
 * The rotation can be any integer, positive, negative, or 0.
 * The function should take the same amount of time to execute for any valid integer input.
 * The function should be callable from a constant context.
 *
 * @example
 *
 *      // Make a 1x3 grid
 *      Grid x(1,3);
 *
 *      // y is size 3x1
 *      Grid y = x.rotate(1);
 *
 * @param _rotation
 *      An positive or negative integer to rotate by in 90 intervals.
 *
 * @return
 *      Returns a copy of the grid that has been rotated.
 */

Grid Grid::rotate(int rotation) const{
    const Grid *old_grid = this; 
    double num_rotation = (rotation % 4 + 4) % 4;
    int oldh = old_grid -> get_height();
    int oldw = old_grid -> get_width();
    Grid y = Grid(old_grid-> get_width(), old_grid-> get_height());
        std::vector<Cell> tvec;
        if (num_rotation == 0){
            y = *old_grid;
            y.gvec = old_grid->gvec;
            return y;
        } 
        if (num_rotation == 1) {
            y = *old_grid;
            y.height = old_grid -> get_width();
            y.width = old_grid -> get_height();
            for (int i = 0; i < oldw; i++){
                for (int j = oldh-1; j >= 0; j--){
                    tvec.push_back(old_grid->gvec[(i+(oldw*j))]);
                }                       
            } 
            y.gvec.clear();
            y.gvec = tvec;
            return y;
        }
        if (num_rotation == 2) {
            y = *old_grid;
            for (int i = oldw*oldh-1; i >= 0; i--){
                tvec.push_back(old_grid->gvec[i]);
            }
            y.gvec.clear();
            y.gvec = tvec;
            return y;
        }
        if (num_rotation == 3) {
            y = *old_grid;
            y.height = old_grid -> get_width();
            y.width = old_grid -> get_height();
            for (int i = oldw-1; i >= 0; i--){
                for (int j = 0; j < oldh; j++){
                    tvec.push_back(old_grid->gvec[(i+(oldw*j))]);
                }                       
            } 
            y.gvec.clear();
            y.gvec = tvec;
            return y;
        }
        if (num_rotation == 4) {
            y = *old_grid;
            y.height = old_grid -> get_width();
            y.width = old_grid -> get_height();
            for (int i = 0; i < oldw; i++){
                for (int j = oldh-1; j >= 0; j--){
                    tvec.push_back(old_grid->gvec[(i+(oldw*j))]);
                }                       
            } 
            y.gvec.clear();
            y.gvec = tvec;
            return y;
        }
        
    return y;
}


/**
 * operator<<(output_stream, grid)
 *
 * Serializes a grid to an ascii output stream.
 * The grid is printed wrapped in a border of - (dash), | (pipe), and + (plus) characters.
 * Alive cells are shown as # (hash) characters, dead cells with ' ' (space) characters.
 *
 * The function should be callable on a constant Grid.
 *
 * @example
 *
 *      // Make a 3x3 grid with a single alive cell
 *      Grid grid(3);
 *      grid(1, 1) = Cell::ALIVE;
 *
 *      // Print the grid to the console
 *      std::cout << grid << std::endl;
 *
 *      // The grid is printed with a border of + - and |
 *
 *      +---+
 *      |   |
 *      | # |
 *      |   |
 *      +---+
 *
 * @param os
 *      An ascii mode output stream such as std::cout.
 *
 * @param grid
 *      A grid object containing cells to be printed.
 *
 * @return
 *      Returns a reference to the output stream to enable operator chaining.
 */
std::ostream& operator<<(std::ostream &stream, const Grid &grid){
    Grid a;
    a = grid;
    std::string b = a.print();
    stream << b;
    return stream;
}

void Grid::make_vector(){
    int vector_size = this->get_total_cells();
    for(int i = 0; i < vector_size; i++){
        gvec.push_back(DEAD);
    }
}

void Grid::set_gvec(std::vector<Cell> temp){
    this->gvec = temp;
}

std::vector<Cell> Grid::get_gvec(){
    return this->gvec;
}

void Grid::set_alive_cells(int x) {
    if (x < 0){
        throw std::invalid_argument("invalid argument");
    }
    this->alive_cells = x;
}

void Grid::set_total_cells(int x) {
    if (x < 0){
        throw std::invalid_argument("invalid argument");
    }
    this->total_cells = x;
}

void Grid::set_dead_cells(int x) {
    if (x < 0){
        throw std::invalid_argument("invalid argument");
    }
    this->dead_cells = x;
}

std::string Grid::print()  {
    std::string ss;
    for (int j = -1; j < this->get_height()+1; j++){
        if ((j == -1) | (j == this->get_height())){
            ss+= "+";
            for (int i = 0; i < this->get_width(); i++){
                ss+= "-";
            }   ss+= "+\n";
        } else {
            ss+= "|";
            for (int i = 0; i < this->get_width(); i++){
                ss += this->gvec[this->get_index(i,j)];
            }
            ss+="|\n";
        }
    }
    return ss;
}
/**
 * Implements a Zoo namespace with methods for constructing Grid objects containing various creatures in the Game of Life.
 *      - Creatures like gliders, light weight spaceships, and r-pentominos can be spawned.
 *          - These creatures are drawn on a Grid the size of their bounding box.
 *
 *      - Grids can be loaded from and saved to an ascii file format.
 *          - Ascii files are composed of:
 *              - A header line containing an integer width and height separated by a space.
 *              - followed by (height) number of lines, each containing (width) number of characters,
 *                terminated by a newline character.
 *              - (space) ' ' is Cell::DEAD, (hash) '#' is Cell::ALIVE.
 *
 *      - Grids can be loaded from and saved to an binary file format.
 *          - Binary files are composed of:
 *              - a 4 byte int representing the grid width
 *              - a 4 byte int representing the grid height
 *              - followed by (width * height) number of individual bits in C-style row/column format,
 *                padded with zero or more 0 bits.
 *              - a 0 bit should be considered Cell::DEAD, a 1 bit should be considered Cell::ALIVE.
 *
 * @author 969197
 * @date March, 2020
 */
#include "zoo.h"
#include <string>
#include <fstream>
#include <bitset>
/**
 * Zoo::glider()
 *
 * Construct a 3x3 grid containing a glider.
 * https://www.conwaylife.com/wiki/Glider
 *
 * @example
 *
 *      // Print a glider in a Grid the size of its bounding box.
 *      std::cout << Zoo::glider() << std::endl;
 *
 *      +---+
 *      | # |
 *      |  #|
 *      |###|
 *      +---+
 *
 * @return
 *      Returns a Grid containing a glider.
 */
Grid Zoo::glider(){
    Grid g = Grid(3);
    g(0,2) = ALIVE;
    g(1,2) = ALIVE;
    g(2,2) = ALIVE;
    g(2,1) = ALIVE;
    g(1,0) = ALIVE;
    return g;
}

/**
 * Zoo::r_pentomino()
 *
 * Construct a 3x3 grid containing an r-pentomino.
 * https://www.conwaylife.com/wiki/R-pentomino
 *
 * @example
 *
 *      // Print an r-pentomino in a Grid the size of its bounding box.
 *      std::cout << Zoo::r_pentomino() << std::endl;
 *
 *      +---+
 *      | ##|
 *      |## |
 *      | # |
 *      +---+
 *
 * @return
 *      Returns a Grid containing a r-pentomino.
 */
Grid Zoo::r_pentomino(){
    Grid g = Grid(3);
    g(1,0) = ALIVE;
    g(2,0) = ALIVE;
    g(0,1) = ALIVE;
    g(1,1) = ALIVE;
    g(1,2) = ALIVE;
    return g;
}

/**
 * Zoo::light_weight_spaceship()
 *
 * Construct a 5x4 grid containing a light weight spaceship.
 * https://www.conwaylife.com/wiki/Lightweight_spaceship
 *
 * @example
 *
 *      // Print a light weight spaceship in a Grid the size of its bounding box.
 *      std::cout << Zoo::light_weight_spaceship() << std::endl;
 *
 *      +-----+
 *      | #  #|
 *      |#    |
 *      |#   #|
 *      |#### |
 *      +-----+
 *
 * @return
 *      Returns a grid containing a light weight spaceship.
 */
Grid Zoo::light_weight_spaceship(){
    Grid g = Grid(5,4);
    g(1,0) = ALIVE;
    g(4,0) = ALIVE;
    g(0,1) = ALIVE;
    g(0,2) = ALIVE;
    g(4,2) = ALIVE;
    g(0,3) = ALIVE;
    g(1,3) = ALIVE;
    g(2,3) = ALIVE;
    g(3,3) = ALIVE;
    return g;
}

/**
 * Zoo::load_ascii(path)
 *
 * Load an ascii file and parse it as a grid of cells.
 * Should be implemented using std::ifstream.
 *
 * @example
 *
 *      // Load an ascii file from a directory
 *      Grid grid = Zoo::load_ascii("path/to/file.gol");
 *
 * @param path
 *      The std::string path to the file to read in.
 *
 * @return
 *      Returns the parsed grid.
 *
 * @throws
 *      Throws std::runtime_error or sub-class if:
 *          - The file cannot be opened.
 *          - The parsed width or height is not a positive integer.
 *          - Newline characters are not found when expected during parsing.
 *          - The character for a cell is not the ALIVE or DEAD character.
 */
Grid Zoo::load_ascii(std::string path){
    std::ifstream inputFile(path);
    if (!inputFile.is_open()) {
        throw std::runtime_error("Could not open file");
    }   
    int width; 
    inputFile >> width;
    int height;
    inputFile >> height;
    if (width < 0 || height < 0){
        throw std::invalid_argument("Negative edges");
    }
    Grid g = Grid(width, height);
    for (int y = 0; y < height; y++){
        for(int x = 0; x <= width; x++){
            char c; 
            c = inputFile.get();
            if (c == '#'){
                g(x-1,y) = ALIVE;
            } else if (!(c == ' ' || c == '\n')){
                throw std::runtime_error("Wrong data");
            }
        }
    }
    inputFile.close();
    return g; 
}

/**
 * Zoo::save_ascii(path, grid)
 *
 * Save a grid as an ascii .gol file according to the specified file format.
 * Should be implemented using std::ofstream.
 *
 * @example
 *
 *      // Make an 8x8 grid
 *      Grid grid(8);
 *
 *      // Save a grid to an ascii file in a directory
 *      try {
 *          Zoo::save_ascii("path/to/file.gol", grid);
 *      }
 *      catch (const std::exception &ex) {
 *          std::cerr << ex.what() << std::endl;
 *      }
 *
 * @param path
 *      The std::string path to the file to write to.
 *
 * @param grid
 *      The grid to be written out to file.
 *
 * @throws
 *      Throws std::runtime_error or sub-class if the file cannot be opened.
 */
void Zoo::save_ascii(std::string path, Grid g){
    std::ofstream outputFile(path);
   if (!outputFile.is_open()) {
        throw std::runtime_error("Could not open file");
    }   
    const int width = g.get_width();
    const int height = g.get_width();
    outputFile << width << " " << height << "\n";

    for (int y = 0; y < height; y++){
        for(int x = 0; x < width; x++){
            if (g(x,y) == ALIVE){
                outputFile << "#";
            } else {
                outputFile << " ";
            }
        } outputFile << "\n";
    }
    outputFile.close();
}

/**
 * Zoo::load_binary(path)
 *
 * Load a binary file and parse it as a grid of cells.
 * Should be implemented using std::ifstream.
 *
 * @example
 *
 *      // Load an binary file from a directory
 *      Grid grid = Zoo::load_binary("path/to/file.bgol");
 *
 * @param path
 *      The std::string path to the file to read in.
 *
 * @return
 *      Returns the parsed grid.
 *
 * @throws
 *      Throws std::runtime_error or sub-class if:
 *          - The file cannot be opened.
 *          - The file ends unexpectedly.
 */
Grid Zoo::load_binary(std::string path){
    int width;
    int height;
    std::ifstream inputFile(path, std::ios_base::binary);
    if (!inputFile.is_open()) {
        throw std::runtime_error("Could not open file");
    }      
    inputFile.read((char*) &width, sizeof(int));
    inputFile.read((char*) &height, sizeof(int));
    std::vector <Cell> temp;
    Grid g = Grid(width, height);
    char c;
    int alive_cells = 0;
    int cell_count = 0;
    while (inputFile.get(c)) {
        for (int i = 0; i < 8; i++){ 
            int x = ((c >> i) & 1);
            if (x == 1) {
                temp.push_back(ALIVE);
                alive_cells++;
                cell_count++;
            } else {
                temp.push_back(DEAD);
                cell_count++;
            }
        }
    }
    if (cell_count < width*height) {
        throw std::runtime_error("File ends unexpectedly");
    }  
    temp.resize(width*height);
    g.set_gvec(temp);
    g.set_alive_cells(alive_cells);
    inputFile.close();
    return g; 
}

/**
 * Zoo::save_binary(path, grid)
 *
 * Save a grid as an binary .bgol file according to the specified file format.
 * Should be implemented using std::ofstream.
 *
 * @example
 *
 *      // Make an 8x8 grid
 *      Grid grid(8);
 *
 *      // Save a grid to an binary file in a directory
 *      try {
 *          Zoo::save_binary("path/to/file.bgol", grid);
 *      }
 *      catch (const std::exception &ex) {
 *          std::cerr << ex.what() << std::endl;
 *      }
 *
 * @param path
 *      The std::string path to the file to write to.
 *
 * @param grid
 *      The grid to be written out to file.
 *
 * @throws
 *      Throws std::runtime_error or sub-class if the file cannot be opened.
 */

void Zoo::save_binary(std::string path, Grid g){
    std::ofstream outputFile(path, std::ofstream::binary);
    if (!outputFile.is_open()) {
        throw std::runtime_error("Could not open file");
    }   
    int height = g.get_height();
    int width = g.get_width();
    outputFile.write((const char*) &height, sizeof(int));
    outputFile.write((const char*) &width, sizeof(int));
    std::vector <Cell> temp = g.get_gvec();
    std::string b = "";
    for (int i = 0; i < height*width;  i = i + 8) {
       std::string b;
        for (int k = 0; k < 8 ; k++){
            if (temp[i+k] == ALIVE){
                b = "1" + b;
            } else if (temp[i+k] == DEAD){
                b = "0" + b;         
            }   
        } 
        outputFile << static_cast<uint8_t>(std::bitset<8>(b).to_ulong());
    } 
    outputFile.close();
}

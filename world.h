/**
 * Declares a class representing a 2d grid world for simulating a cellular automaton.
 * Rich documentation for the api and behaviour the World class can be found in world.cpp.
 *
 * The test suites provide granular BDD style (Behaviour Driven Development) test cases
 * which will help further understand the specification you need to code to.
 *
 * @author 969197
 * @date March, 2020
 */
#include "grid.h"
#include <iostream>   
#include <vector>

#pragma once

/**
 * Declare the structure of the World class for representing a 2d grid world.
 *
 * A World holds two equally sized Grid objects for the current state and next state.
 *      - These buffers should be swapped using std::swap after each update step.
 */
class World {
    private:
        int count_neighbours(int x, int y, Grid g, bool toroidal);
    public:
        Grid c_grid;
        Grid n_grid;
        World();
        World(int square_size);
        World(int width, int height);
        World(Grid w_grid);
        bool in_bounds(int x, int y);
        const int get_width() const;
        const int get_height() const;
        const int get_total_cells() const;
        const int get_alive_cells() const;
        const int get_dead_cells() const;
        void resize (int width, int height);
        void resize (int square_size);
        const Grid get_state() const;
        void step(bool toroidal);
        void step();
        void advance(int steps, bool toroidal);
        void advance(int steps);
};
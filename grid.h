/**
 * Declares a class representing a 2d grid of cells.
 * Rich documentation for the api and behaviour the Grid class can be found in grid.cpp.
 *
 * The test suites provide granular BDD style (Behaviour Driven Development) test cases
 * which will help further understand the specification you need to code to.
 *
 * @author 969197
 * @date March, 2020
 */

#include <vector>
#include <ostream>   
#include <iostream>   

#pragma once
// Add the minimal number of includes you need in order to declare the class.
// #include ...

/**
 * A Cell is a char limited to two named values for Cell::DEAD and Cell::ALIVE.
 */
enum Cell : char {
    DEAD  = ' ',
    ALIVE = '#'
};

/**
 * Declare the structure of the Grid class for representing a 2d grid of cells.
 */
class Grid {
    private:
        int width;
        int height;
        int alive_cells;
        int dead_cells;
        int total_cells;
        void make_vector();
        const int get_index(int x, int y) const;
        std::string print();
        std::vector<Cell> gvec;
    public:
        Grid();
        Grid(int width, int height);
        Grid(int square);
        const int get_width() const;
        const int get_height() const;
        const int get_total_cells() const;
        const int get_alive_cells() const;
        const int get_dead_cells() const;
        void resize (int width, int height);
        void resize (int square_size);
        Grid crop (int x0, int y0, int x1, int y1);
        void set (int x, int y, Cell value);
        void set_gvec(std::vector<Cell> temp);
        std::vector<Cell> get_gvec();
        const Cell get(int x, int y) const;
        void merge(Grid other, int x0, int y0, bool alive_only);
        void merge(Grid other, int x0, int y0);
        Grid rotate(int rotation) const;
        void set_alive_cells(int x);
        void set_dead_cells(int x);
        void set_total_cells(int x);
        friend std::ostream& operator<<(std::ostream &stream, const Grid &grid);
        Cell& operator()(int x, int y);
        const Cell& operator()(int x, int y) const;
};
